import java.util.Random;
public class Board{
	//fields
	private Tile[][] grid;
	final int rows = 5;
	final int columns = 5;
	//constructor
	public Board(){
		Random rng = new Random();
		grid = new Tile[rows][columns];
		//initialize each Tile inside grid to "_"
		for(int r=0; r<grid.length; r++){
			for(int col=0; col<grid[r].length; col++)
				grid[r][col] = Tile.BLANK;
			//put a random position in each row to HIDDEN_WALL
			grid[r][rng.nextInt(grid[r].length)] = Tile.HIDDEN_WALL;
		}
		
	}
	//methods
	public String toString(){
		String result = " ";
		for (int i=0; i<this.columns; i++)
				result += "  " + i;
		
		for(int r=0; r<grid.length; r++){
			result += "\n" + r + "  ";
			for(int col=0; col<grid[r].length; col++)
				result += grid[r][col].getName() + "  "; 
		}
		return result;
	}
	/*method check if given row and col is valid to play
	* if tile == BLANK --> assign + return true
	* if tile != BLANK --> return false
	*/
	public int placeToken(int row, int col){
		//check if row && col are in correct range 
		if ((row>=0 && row<this.rows) && (col>=0 && col<this.columns)){
			//check if a tile is equal to BLANK
			if (grid[row][col] == Tile.BLANK){
				grid[row][col] = Tile.CASTLE;
				return 0;
			} 
			else
			if (grid[row][col] == Tile.CASTLE ||
				grid[row][col] == Tile.WALL)
				return -1; 
			else {
				grid[row][col] = Tile.WALL;
				return 1;
			}
		} else
			return -2;
	}
	
}