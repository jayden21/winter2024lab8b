import java.util.Scanner; 

public class BoardGameApp{
	public static void main(String[] args){
		Scanner reader = new Scanner (System.in);
		Board b = new Board();
		System.out.println("Welcome to Board Game!");
		int numCastles = 6;
		int turns = 0;
		final int maxTurn = 8;
		int row, col, result;
		
		while (numCastles > 0 && turns < maxTurn){
			System.out.println(b.toString() + "\n" +
							   "Number of castles: " + numCastles + "\n" +
							   "Turns: " + turns);
			System.out.println("Enter 2 numbers representing the row and column.");
			System.out.print("Row: ");
			row = reader.nextInt();
			System.out.print("Column: ");
			col = reader.nextInt();
			result = b.placeToken(row, col);
			while (result < 0){
				System.out.println("Please re-enter the row and column");
				System.out.print("Row: ");
				row = reader.nextInt();
				System.out.print("Column: ");
				col = reader.nextInt(); 
				result = b.placeToken(row, col);
			}
			if (result == 1){
				System.out.println("\n" + "*** There was a wall at that position! ***" + "\n");
				turns ++;
			}
			if (result == 0){
				System.out.println("\n" + "*** A castle tile was successfully placed! ***" + "\n");
				turns ++; 
				numCastles --;
			}
			
		}
		System.out.println(b.toString());
		if (numCastles == 0)
			System.out.println("You won!");
		else 
			System.out.println("You lost!");
		
	
	}
}