enum Tile{
	BLANK("_"),
	WALL("W"),
	HIDDEN_WALL("_"),
	CASTLE("C");
	
	final String name;
	//constructor
	private Tile(String name){
		this.name = name;
	}
	//getter
	public String getName(){
		return this.name;
	}
}

